package com.somecompany.satellite.tools;

import com.somecompany.satellite.domain.TelemetryData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DataIngesterTests {
    @Autowired
    private IDataIngester dataIngester;

    @Value("classpath:satellite-data.txt")
    private Resource satelliteData;

    @Value("${satellite.timestamp.pattern}")
    private String timestampPattern;

    @Test
    public void DataIngester_OnSuccess_ReturnsData() throws  Exception {
        assertNotNull(timestampPattern);
        assertTrue(satelliteData.exists());
        List<TelemetryData> data = dataIngester.consume(satelliteData.getFile());
        assertNotNull(data);
        TelemetryData d = data.get(0);
        assertNotNull(d.getTimestamp());
        assertTrue(1001 == d.getId());
        assertTrue(101 == d.getRedHighLimit());
        assertTrue(98 == d.getYellowHighLimit());
        assertTrue(25 == d.getYellowLowLimit());
        assertTrue(20 == d.getRedLowLimit());
        assertTrue(99.9 == d.getRawValue());
        assertTrue("TSTAT".equals(d.getComponent()));
    }

    @Test
    public void TextFile_OnParse_ReturnsArray() {
        String testData = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";
        String[] d = testData.split("\\|");
        for(String s : d) {
            System.out.println(s);
        }
        System.out.println(d[TelemetryDataPosition.TIME_STAMP]);
        assertTrue("20180101 23:01:05.001".equals(d[TelemetryDataPosition.TIME_STAMP]));
        assertTrue("1001".equals(d[TelemetryDataPosition.SATELLITE_ID]));
        assertTrue("101".equals(d[TelemetryDataPosition.RED_HIGH_LIMIT]));
        assertTrue("98".equals(d[TelemetryDataPosition.YELLOW_HIGH_LIMIT]));
        assertTrue("25".equals(d[TelemetryDataPosition.YELLOW_LOW_LIMIT]));
        assertTrue("20".equals(d[TelemetryDataPosition.RED_LOW_LIMIT]));
        assertTrue("99.9".equals(d[TelemetryDataPosition.RAW_VALUE]));
        assertTrue("TSTAT".equals(d[TelemetryDataPosition.COMPONENT]));
    }

    @Test
    public void DateTimeFormatter_OnParse_ReturnsDate() {
        assertNotNull(timestampPattern);
        System.out.println(timestampPattern);
        String sampleDateString = "20180101 23:01:05.001";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timestampPattern);
        LocalDateTime ldt = LocalDateTime.from(formatter.parse(sampleDateString));
        assertTrue(ldt.getHour() == 23);
        assertTrue(ldt.getMinute() == 1);
        assertTrue(ldt.getSecond() == 5);
    }

    @Test
    public void File_OnParse_ReturnsLines() throws IOException {
        File f = satelliteData.getFile();
        BufferedReader br = new BufferedReader(new FileReader(f));
        String line = null;
        while((line = br.readLine()) != null) {
            assertNotNull(line);
            System.out.println(line);
        }
    }
}
