package com.somecompany.satellite.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somecompany.satellite.domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class AlertMonitorTests {

    @Value("classpath:satellite-data.txt")
    private Resource satelliteData;

    @Value("classpath:satellite-data1.txt")
    private Resource moreSatelliteData;

    @Value("${satellite.timestamp.pattern}")
    private String timestampPattern;
    @Value("${satellite.data.delimiter}")
    private String delimiter;

    @Value("${satellite.data.interval.duration}")
    private int intervalDuration;

    @Autowired
    private DateTimeFormatter dateTimeFormatter;

    @Autowired
    private IDataIngester dataIngester;


    @Test
    public void DateTimeString_WhenConverted_IsZonedDateTimeString() {
        String t = "20180101 23:01:09.521";
        DateTimeFormatter formatter
                = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
        //ZonedDateTime.parse(t, formatter);
        LocalDateTime ldt = LocalDateTime.parse(t, formatter);
        ZonedDateTime zdt = ldt.atZone(ZoneOffset.UTC);
        System.out.println(zdt.format(DateTimeFormatter.ISO_INSTANT));
    }

    @Test
    public void BatteryTelemetryData_OnAlertTriggered_ReturnsRedLowLimit() {
        String tData = "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT";
        TelemetryData td = new TelemetryData(tData, dateTimeFormatter, delimiter);
        assertTrue(td.hasViolation());
        assertTrue(ViolationType.RedLowLimit == td.getViolation());
    }

    @Test
    public void BatteryTelemetryData_WhenCheckingAlert_ReturnsNoAlert() {
        String tData = "20180101 23:01:09.521|1000|17|15|9|8|9|BATT";
        TelemetryData td = new TelemetryData(tData, dateTimeFormatter, delimiter);
        assertFalse(td.hasViolation());
        assertTrue(ViolationType.NoViolation == td.getViolation());

    }

    @Test
    public void TelemetryData_OnAlert_ReturnsRedHighAlert() {
        String tData = "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT";
        TelemetryData td = new TelemetryData(tData, dateTimeFormatter, delimiter);
        assertTrue(td.hasViolation());
        assertTrue(ViolationType.RedHighLimit == td.getViolation());

    }

    @Test
    public void TelemetryData_OnAlert_ReturnsRedLowAlert() {

        String tData = "20180101 23:01:38.001|1000|101|98|25|20|99|TSTAT";
        TelemetryData td = new TelemetryData(tData, dateTimeFormatter, delimiter);
        assertFalse(td.hasViolation());
        assertTrue(ViolationType.NoViolation == td.getViolation());

    }

    @Test
    public void AlertMonitor_WhenFinished_DisplaysThreeIntervals() throws IOException {

        assertTrue(moreSatelliteData.getFile().exists());

        AlertMonitor alertMonitor = new AlertMonitor(intervalDuration, dateTimeFormatter, delimiter);
        alertMonitor.init();
        alertMonitor.addObserver(new JsonAlertObserver());

        List<TelemetryData> data = dataIngester.consume(moreSatelliteData.getFile());
        for(TelemetryData td : data) {
            alertMonitor.receiveTelemetry(td);
        }
        assertTrue(alertMonitor.getIntervalCount() == 2);
    }

    @Test
    public void DataIngestion_WhenParsedAndProcessed_DisplaysTwoIntervals() throws IOException {
        assertTrue(satelliteData.getFile().exists());
        List<TelemetryData> data = dataIngester.consume(moreSatelliteData.getFile());
        int intervalStart = 0;
        int intervalEnd = 300;
        int seconds = 0;

        int intervalCounter = 0;

        for(TelemetryData td : data) {
            if (intervalCounter == 12) {
                intervalStart = 0;
                intervalEnd = 300;
                intervalCounter = 0;
            }
            seconds = ((td.getTimestamp().getMinute()*60) + td.getTimestamp().getSecond());
//            if (seconds >= intervalStart && seconds <= intervalEnd) {
//                System.out.println(seconds);
//                System.out.println("Checking the alert");
//            }
//            else {
//                intervalCounter++;
//                System.out.println(String.format("%s interval reached", intervalCounter));
//                System.out.println("Sending accumulated alerts");
//                System.out.println("Clearing the alerts");
//                intervalStart = intervalEnd;
//                intervalEnd += 300;
//                System.out.println("Interval Start: " + intervalStart);
//                System.out.println("Interval End: " + intervalEnd);
//                System.out.println(seconds);
//                System.out.println("Checking the alert");
//            }

            if (seconds >= intervalEnd) {
                intervalCounter++;
                System.out.println(String.format("%s interval reached", intervalCounter));
                System.out.println("Sending accumulated alerts");
                System.out.println("Clearing the alerts");
                intervalStart = intervalEnd;
                intervalEnd += 300;
                System.out.println("Interval Start: " + intervalStart);
                System.out.println("Interval End: " + intervalEnd);
            }
            System.out.println(seconds);
            System.out.println("Checking the alert");
        }

    }

    @Test
    public void AlertNotification_WhenInitialized_ReturnsJson() throws JsonProcessingException {
        AlertNotification an = new AlertNotification();
        an.setComponent(ComponentType.BATTERY);
        an.setSatelliteId(1001);
        an.setSeverity(ViolationType.SEVERITY.get(ViolationType.RedLowLimit));
        an.setTimestamp("20180101 23:04:11.531");
        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(an);
        System.out.println(json);
        assertNotNull(json);
    }
}
