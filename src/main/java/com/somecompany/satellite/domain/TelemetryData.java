package com.somecompany.satellite.domain;

import com.somecompany.satellite.tools.TelemetryDataPosition;
import lombok.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@ToString
public class TelemetryData {
    private LocalDateTime timestamp;
    private String originalTimestamp;

    private int id;
    private double redHighLimit;
    private double yellowHighLimit;
    private double yellowLowLimit;
    private double redLowLimit;
    private double rawValue;
    private String component;
    private ViolationType violation;

    private String timestampPattern;
    private String delimiter;

    private DateTimeFormatter dateTimeFormatter;

    public TelemetryData(String rawData, DateTimeFormatter dateTimeFormatter, String delimiter) {
        this.setDateTimeFormatter(dateTimeFormatter);
        this.setDelimiter(delimiter);
        this.setRawData(rawData);
    }

    private void setRawData(String rawData) {
        String[] d = rawData.split(delimiter);
        this.setOriginalTimestamp(d[TelemetryDataPosition.TIME_STAMP]);
        this.setTimestamp(LocalDateTime.from(this.getDateTimeFormatter().parse(d[TelemetryDataPosition.TIME_STAMP])));
        this.setId(Integer.parseInt(d[TelemetryDataPosition.SATELLITE_ID]));
        this.setRedHighLimit(redHighLimit = Double.parseDouble(d[TelemetryDataPosition.RED_HIGH_LIMIT]));
        this.setYellowHighLimit(Double.parseDouble(d[TelemetryDataPosition.YELLOW_HIGH_LIMIT]));
        this.setYellowLowLimit(Double.parseDouble(d[TelemetryDataPosition.YELLOW_LOW_LIMIT]));
        this.setRedLowLimit(Double.parseDouble(d[TelemetryDataPosition.RED_LOW_LIMIT]));
        this.setRawValue(Double.parseDouble(d[TelemetryDataPosition.RAW_VALUE]));
        this.setComponent(d[TelemetryDataPosition.COMPONENT]);
        this.setViolation(ViolationType.NoViolation);
    }

    public boolean hasViolation() {
        boolean isViolation = false;
        if ((this.rawValue < redLowLimit) && ComponentType.BATTERY.equalsIgnoreCase(component)) {
            this.setViolation(ViolationType.RedLowLimit);
            isViolation = true;
        }

        if ((this.rawValue > redHighLimit) && ComponentType.THERMOSTAT.equalsIgnoreCase(component)) {
            this.setViolation(ViolationType.RedHighLimit);
            isViolation = true;
        }
        return isViolation;
    }

}
