package com.somecompany.satellite.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Getter
@Setter
@ToString
public class AlertNotification {
    public AlertNotification() {
    }
    public void init(TelemetryData td) {
        if (td != null) {
            this.setTelemetryData(td);
            this.satelliteId = td.getId();
            this.severity = td.hasViolation() ? ViolationType.SEVERITY.get(td.getViolation()) : "NO ALERT";
            this.component = td.getComponent().toUpperCase();
            LocalDateTime ldt = td.getTimestamp();
            ZonedDateTime zdt = ldt.atZone(ZoneOffset.UTC);
            this.timestamp = zdt.format(DateTimeFormatter.ISO_INSTANT);
        }
    }

    @JsonIgnore
    private TelemetryData telemetryData;
    private int satelliteId;
    private String severity;
    private String component;
    private String timestamp;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlertNotification that = (AlertNotification) o;
        return satelliteId == that.satelliteId && severity.equals(that.severity) && component.equals(that.component) && timestamp.equals(that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, severity, component, timestamp);
    }
}

