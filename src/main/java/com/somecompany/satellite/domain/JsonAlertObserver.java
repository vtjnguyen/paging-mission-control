package com.somecompany.satellite.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class JsonAlertObserver implements Observer {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Override
    public void update(Observable o, Object alerts) {
        Set<AlertNotification> a = (HashSet<AlertNotification>)alerts;
        ObjectMapper om = new ObjectMapper();

        try {
            String alertsAsJson = om.writerWithDefaultPrettyPrinter().writeValueAsString(a);
            log.info("\n" + alertsAsJson);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
