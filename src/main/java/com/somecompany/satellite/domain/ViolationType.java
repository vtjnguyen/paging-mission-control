package com.somecompany.satellite.domain;

import java.util.HashMap;
import java.util.Map;

public enum ViolationType {
    RedHighLimit, YellowHighLimit, YellowLowLimit, RedLowLimit, NoViolation;
    public static Map<ViolationType, String> SEVERITY = new HashMap(){
        {
            put(ViolationType.RedHighLimit, "RED HIGH");
            put(ViolationType.RedLowLimit, "RED LOW");
            put(ViolationType.YellowHighLimit, "YELLOW HIGH");
            put(ViolationType.YellowLowLimit, "YELLOW LOW");
        }
    };
}
