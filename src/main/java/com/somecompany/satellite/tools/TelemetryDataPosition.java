package com.somecompany.satellite.tools;

public class TelemetryDataPosition {
    public static int TIME_STAMP = 0;
    public static int SATELLITE_ID = 1;
    public static int RED_HIGH_LIMIT = 2;
    public static int YELLOW_HIGH_LIMIT = 3;
    public static int YELLOW_LOW_LIMIT = 4;
    public static int RED_LOW_LIMIT = 5;
    public static int RAW_VALUE = 6;
    public static int COMPONENT = 7;
}
