package com.somecompany.satellite.tools;

import com.somecompany.satellite.domain.AlertNotification;
import com.somecompany.satellite.domain.ComponentType;
import com.somecompany.satellite.domain.TelemetryData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class AlertMonitor extends Observable {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final String delimiter;

    private final DateTimeFormatter dateTimeFormatter;

    private Map<String, List<TelemetryData>> telemetryDataViolationMap = new HashMap<>();

    private Set<AlertNotification> alerts = new HashSet<>();
    private int intervalStart = 0;
    private int intervalEnd;

    private final int intervalDuration;

    private int intervalCounter = 0;

    private int maxIntervalInAnHour;

    public AlertMonitor(int intervalDuration, DateTimeFormatter dateTimeFormatter, String delimiter) {
        this.intervalDuration = intervalDuration;
        this.dateTimeFormatter = dateTimeFormatter;
        this.delimiter = delimiter;
    }

    public void init() {
        this.maxIntervalInAnHour = 3600/this.intervalDuration;
        this.intervalStart = 0;
        this.intervalEnd = this.intervalDuration;
    }

    private void sendAlerts() {
        setChanged();
        notifyObservers(this.alerts);
    }

    public int getIntervalCount() {
        return this.intervalCounter;
    }

    public void receiveTelemetry(String s) {
        TelemetryData td = new TelemetryData(s, dateTimeFormatter, delimiter);
        this.receiveTelemetry(td);
    }

    private void addAlertIfViolationsCountIsTrueForComponent(String componentKey) {
        if (this.telemetryDataViolationMap.containsKey(componentKey)
                && this.telemetryDataViolationMap.get(componentKey).size() == 3)
        {
            // only getting the first occurrence of a violation
            TelemetryData firstAlert = this.telemetryDataViolationMap.get(componentKey).get(0);
            AlertNotification an = new AlertNotification();
            an.init(firstAlert);
            alerts.add(an);
        }
    }

    public void receiveTelemetry(TelemetryData telemetryData) {
        if (intervalCounter == this.maxIntervalInAnHour) {
            intervalStart = 0;
            intervalEnd = intervalDuration;
            intervalCounter = 0;
        }

        int seconds = ((telemetryData.getTimestamp().getMinute() * 60) + telemetryData.getTimestamp().getSecond());
        if (seconds >= intervalEnd) {
            intervalCounter++;
            log.debug(String.format("%s interval reached", intervalCounter));
            log.debug("Sending any alerts");

            if (alerts.size() > 0) {
                sendAlerts();
                // resetting the alerts and triggers for the next interval
                alerts = new HashSet<>();
                telemetryDataViolationMap = new HashMap<>();
            }

            log.debug("Clearing the alerts");
            intervalStart = intervalEnd;
            intervalEnd += intervalDuration;
            log.debug("Interval Start: " + intervalStart);
            log.debug("Interval End: " + intervalEnd);
        }

        String componentKey = telemetryData.getComponent() + "|" + telemetryData.getId();

        if (!telemetryDataViolationMap.containsKey(componentKey)) {
            telemetryDataViolationMap.put(componentKey, new ArrayList<>());
        }

        log.debug("Checking for violation at: " + seconds);
        if (telemetryData.hasViolation()) {
            telemetryDataViolationMap.get(componentKey).add(telemetryData);
        }

        String thermostatKey = ComponentType.THERMOSTAT + "|" + telemetryData.getId();
        addAlertIfViolationsCountIsTrueForComponent(thermostatKey);

        String batteryKey = ComponentType.BATTERY + "|" + telemetryData.getId();
        addAlertIfViolationsCountIsTrueForComponent(batteryKey);
    }
}
