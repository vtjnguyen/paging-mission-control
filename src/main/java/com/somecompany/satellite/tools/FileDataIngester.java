package com.somecompany.satellite.tools;

import com.somecompany.satellite.domain.TelemetryData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class FileDataIngester implements IDataIngester {

    @Value("${satellite.data.delimiter}")
    private String delimiter;

    @Autowired
    private DateTimeFormatter dateTimeFormatter;

    @Override
    public List<TelemetryData> consume(File dataFile) throws IOException {
        if (dataFile == null || !dataFile.exists()) {
            return null;
        }

        List<TelemetryData> data = new ArrayList<>();
        TelemetryData td = null;
        try (BufferedReader br = new BufferedReader(new FileReader(dataFile))) {

            String line = null;
            while((line = br.readLine()) != null) {
                td = new TelemetryData(line, dateTimeFormatter, delimiter);
                data.add(td);
            }
        }

        return data;
    }
}
