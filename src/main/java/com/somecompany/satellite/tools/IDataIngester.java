package com.somecompany.satellite.tools;

import com.somecompany.satellite.domain.TelemetryData;

import java.io.File;
import java.io.IOException;
import java.util.List;


public interface IDataIngester {
    public List<TelemetryData> consume(File dataFile) throws IOException;
}


