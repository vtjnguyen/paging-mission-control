package com.somecompany.satellite;

import com.somecompany.satellite.domain.JsonAlertObserver;
import com.somecompany.satellite.tools.AlertMonitor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
public class SatelliteApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SatelliteApplication.class, args);
    }

    @Value("${satellite.timestamp.pattern}")
    private String timestampPattern;

    @Value("${satellite.data.delimiter}")
    private String delimiter;

    @Value("${satellite.data.interval.duration}")
    private int intervalDuration;
    @Value("classpath:satellite-data.txt")
    private Resource satelliteData;

    @Bean
    public DateTimeFormatter getDateTimeFormatter() {
        return DateTimeFormatter.ofPattern(timestampPattern);
    }

    @Override
    public void run(String... args) throws Exception {
        AlertMonitor alertMonitor = new AlertMonitor(intervalDuration, getDateTimeFormatter(), delimiter);
        alertMonitor.init();

        // Can create multiple observers to send the alerts in different formats or to devices.
        alertMonitor.addObserver(new JsonAlertObserver());

        try (BufferedReader br = new BufferedReader(new FileReader(satelliteData.getFile()))) {
            String line;

            while((line = br.readLine()) != null) {
                alertMonitor.receiveTelemetry(line);
            }
        }
    }
}
